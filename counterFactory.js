
function counterFactory() {
    let counterVal = 0;

    return {
        increment: function(){
            return counterVal += 1;
        },

        decrement: function(){
            return counterVal -= 1;
        }
    }
};

module.exports = { counterFactory };