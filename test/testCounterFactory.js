const testCounterFactoryModule = require('../counterFactory');

const result = testCounterFactoryModule.counterFactory();

console.log(result.increment());    // 1
console.log(result.increment());    // 2

console.log(result.decrement());    // 1

console.log(result.increment());    // 2
console.log(result.increment());    // 3

