const cacheFunctionModule = require('../cacheFunction');

function cb(a,b,c) {
    return a+b+c;
}

const result = cacheFunctionModule.cacheFunction(cb);

console.log(result(1,2,3));
console.log(result(1,2,3));
console.log(result(4,3,2));
console.log(result(4,3,2));