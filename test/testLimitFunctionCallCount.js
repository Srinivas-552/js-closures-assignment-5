const functionCallCountModule = require('../limitFunctionCallCount');


function cb() {
    console.log("Hello World");
}

const result = functionCallCountModule.limitFunctionCallCount(cb, 3);

result();
result();
result();
result();   // No output
result();   // No output