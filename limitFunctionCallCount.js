
function limitFunctionCallCount(cb, n) {
    let count = 1;
    
    return function() {
        console.log(count)
        return count++ <= n ? cb() : null;
    }
    
};

module.exports = { limitFunctionCallCount };