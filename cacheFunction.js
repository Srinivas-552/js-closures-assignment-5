
function cacheFunction(cb) {

    const cache = {};
    return function invokeCbFn(...args) {
        const argsKey = JSON.stringify(args);
        console.log(argsKey)
        if (!cache[argsKey]) {
            cache[argsKey] = cb(...args);
        }else {
            return cache[argsKey];
        }
        
    }
    
};

module.exports = {cacheFunction}